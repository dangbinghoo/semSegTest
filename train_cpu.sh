#!/bin/sh
python train.py --dataset wm_pascal --model psp --backbone resnet50 --epochs 2000 --lr 0.01 --checkname res50 --syncbn --workers 2 --batch-size 2 --no-cuda
