#!/bin/sh
python train.py --dataset wm_pascal --model psp --backbone resnet101 --epochs 200 --lr 0.01 --checkname res101 --syncbn --workers 2 --batch-size 2   --resume runs/wm_pascal/psp/res101/checkpoint.params 
