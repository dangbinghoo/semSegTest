#!/bin/sh
python test.py --dataset wm_pascal --model psp --backbone resnet101 --lr 0.01 --workers 2 --resume runs/wm_pascal/psp/res101/checkpoint.params 
