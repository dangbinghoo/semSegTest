import os
from tqdm import tqdm
import numpy as np

import mxnet as mx
from mxnet import gluon, image
from mxnet.gluon.data.vision import transforms

import gluoncv
from gluoncv.model_zoo.segbase import get_segmentation_model, MultiEvalModel
from gluoncv.model_zoo import get_model, get_psp, PSPNet
from gluoncv.data import get_segmentation_dataset, ms_batchify_fn
from gluoncv.utils.viz import get_color_pallete
import matplotlib.image as mpimg
from matplotlib import pyplot as plt

from mypascal_voc.segmentation import WMVOCSegmentation

def predict_wm(filename, weight):
    ctx = mx.gpu(0)
    #ctx = mx.cpu(0)
    # output folder
    outdir = 'outdir'
    NUM_CLASS = 2
    syncbn = False
    ngpus = 1
    
    input_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([.485, .456, .406], [.229, .224, .225]),
    ])

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # create network
    print('Loading PSPNet......')
    
    dataset = WMVOCSegmentation()
    model = PSPNet(dataset.NUM_CLASS, backbone='resnet50', ctx=[ctx], pretrained_base=False, aux=False)

    # norm_layer = mx.gluon.contrib.nn.SyncBatchNorm if syncbn \
    #     else mx.gluon.nn.BatchNorm
    # norm_kwargs = {'num_devices': ngpus} if syncbn else {}
    # model = get_segmentation_model(model='psp', dataset='wm_pascal', ctx=[ctx], \
    #    backbone='resnet50', norm_layer=norm_layer, norm_kwargs={}, aux=False, base_size=520, crop_size=480)

    if os.path.isfile(weight):
        model.load_parameters(weight, ctx=[ctx])
    else:
        raise RuntimeError("=> no checkpoint found at '{}'" \
            .format(weight))
#    print(model)


# =============================================================================
#  Multi prediction Test
    outdir = 'outdir'
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    test_batch_size=16

    testset = get_segmentation_dataset(
            'wm_pascal', split='test', mode='test', transform=input_transform)
    print(testset.images)
    print(testset.masks)
    test_data = gluon.data.DataLoader(
        testset, test_batch_size, shuffle=False, last_batch='keep',
        batchify_fn=ms_batchify_fn, num_workers=2)

    evaluator = MultiEvalModel(model, testset.num_class, ctx_list=[ctx])

    tbar = tqdm(test_data)
    for i, (data, dsts) in enumerate(tbar):
        im_paths = dsts
        predicts = evaluator.parallel_forward(data)
        for predict, impath in zip(predicts, im_paths):
            print('img-path ', impath)
            predict = mx.nd.squeeze(mx.nd.argmax(predict, 1)).asnumpy() + \
                testset.pred_offset
            mask = get_color_pallete(predict, 'wm_pascal')
            outname = os.path.splitext(impath)[0] + '.png'
            mask.save(os.path.join(outdir, outname))
#=============================================================================

#=============================================================================
    # print('read image ....')
    # # load the image
    # img = image.imread(filename)
    # plt.imshow(img.asnumpy())
    # plt.show()
    
    # # normalize the image using dataset mean
    # img = input_transform(img)
    # img = img.expand_dims(0).as_in_context(ctx)

    # print('Doing predict ....')
    # output = model.demo(img)
    # predict = mx.nd.squeeze(mx.nd.argmax(output, 1)).asnumpy()
    # plt.imshow(predict)
    # plt.show()
    
    # mask = get_color_pallete(predict, 'wm_pascal')
    # mask.save('predict-out.png')


    # mmask = mpimg.imread('predict-out.png')
    # plt.imshow(mmask)
    # plt.show()
# 
# =============================================================================
    



if __name__ == "__main__":
    predict_wm('./testJPGs/3.jpg', 'checkpoint-res50-epoch1999-0802.params')
